package ru.zorin.tm.bootstrap;
import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.api.repository.IUserRepository;
import ru.zorin.tm.api.service.*;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.command.auth.CommandLogin;
import ru.zorin.tm.command.auth.CommandLogout;
import ru.zorin.tm.command.auth.CommandRegistry;
import ru.zorin.tm.command.project.*;
import ru.zorin.tm.command.system.AboutCommand;
import ru.zorin.tm.command.system.ExitCommand;
import ru.zorin.tm.command.system.HelpCommand;
import ru.zorin.tm.command.system.SystemInfoCommand;
import ru.zorin.tm.command.task.*;
import ru.zorin.tm.command.user.*;
import ru.zorin.tm.error.invalid.InvalidCommandException;
import ru.zorin.tm.repository.ProjectRepository;
import ru.zorin.tm.repository.TaskRepository;
import ru.zorin.tm.repository.UserRepository;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.service.*;
import ru.zorin.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        registryCommand(new HelpCommand());
        registryCommand(new SystemInfoCommand());
        registryCommand(new AboutCommand());
        registryCommand(new CommandLogin());
        registryCommand(new CommandLogout());
        registryCommand(new CommandRegistry());
        registryCommand(new UserShowByIdCommand());
        registryCommand(new UserShowByLoginCommand());
        registryCommand(new UserUpdateByIdCommand());
        registryCommand(new UserUpdateByLoginCommand());
        registryCommand(new UserRemoveByIdCommand());
        registryCommand(new UserRemoveByLoginCommand());
        registryCommand(new TaskCreateCommand());
        registryCommand(new TaskShowListCommand());
        registryCommand(new TaskShowByIdCommand());
        registryCommand(new TaskShowByNameCommand());
        registryCommand(new TaskShowByIndexCommand());
        registryCommand(new TaskUpdateByIdCommand());
        registryCommand(new TaskRemoveByIndexCommand());
        registryCommand(new TaskRemoveByIdCommand());
        registryCommand(new TaskRemoveByNameCommand());
        registryCommand(new TaskClearCommand());
        registryCommand(new ProjectCreateCommand());
        registryCommand(new ProjectShowListCommand());
        registryCommand(new ProjectShowByIdCommand());
        registryCommand(new ProjectShowByNameCommand());
        registryCommand(new ProjectShowByIndexCommand());
        registryCommand(new ProjectUpdateByIdCommand());
        registryCommand(new ProjectRemoveByIndexCommand());
        registryCommand(new ProjectRemoveByIdCommand());
        registryCommand(new ProjectRemoveByNameCommand());
        registryCommand(new ExitCommand());
    }

    private void registryCommand(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    {
        registryArg(new HelpCommand());
        registryArg(new SystemInfoCommand());
        registryArg(new AboutCommand());
    }

    private void registryArg(final AbstractCommand argument) {
        if (argument == null) return;
        argument.setServiceLocator(this);
        arguments.put(argument.arg(), argument);
    }

    private void initUser() {
        userService.create("test", "test", "test@email.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("***WELCOME TO TASK MANAGER***");
        initUser();
        parseArg(args);
        inputCommand();
    }

private void inputCommand(){
    while (true) {
        try {
            parseCommand(TerminalUtil.nextLine());
        } catch (Exception e) {
            System.err.println("[ERROR]");
            System.err.println(e.getMessage());
        }
    }
}
    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new InvalidCommandException();
        command.execute();
    }

    private boolean parseArg(final String... args) {
        if (args == null || args.length == 0) return false;
        for (String arg : args) validateArg(arg);
        return true;
    }

    private void validateArg(final String args) {
        final AbstractCommand argument = arguments.get(args);
        if (arguments.get(args) == null) throw new InvalidCommandException();
        argument.execute();
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands(){
        return commands.values();
    }

    public Collection<AbstractCommand> getArgs(){
        return arguments.values();
    }
}