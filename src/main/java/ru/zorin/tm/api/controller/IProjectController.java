package ru.zorin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();
}
