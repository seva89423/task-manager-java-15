package ru.zorin.tm.service;

import ru.zorin.tm.api.service.IAuthService;
import ru.zorin.tm.api.service.IUserService;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.access.AccessDeniedException;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.error.invalid.InvalidPasswordException;
import ru.zorin.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new InvalidLoginException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new InvalidPasswordException();
        if(!hash.equals(user.getPasswordHash())) throw new InvalidPasswordException();
        userId = user.getId();

    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }
}
