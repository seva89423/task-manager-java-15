package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.error.task.TaskUpdateException;
import ru.zorin.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-task-by-id";
    }

    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) throw new TaskEmptyException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
        if (taskUpdated == null) throw new TaskUpdateException();
        System.out.println("[COMPLETE]");
    }
}