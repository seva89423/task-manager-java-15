package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Clear list of task";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[COMPLETE]");
    }
}