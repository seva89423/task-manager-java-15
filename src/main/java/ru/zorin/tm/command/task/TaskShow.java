package ru.zorin.tm.command.task;

import ru.zorin.tm.entity.Task;

public class TaskShow {
    static void showTask(Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }
}
