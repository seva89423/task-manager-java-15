package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-task-by-id";
    }

    @Override
    public String description() {
        return "Show task-by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) {
            System.out.println("[ERROR]");
            return;
        }
        TaskShow.showTask(task);
        System.out.println("[COMPLETE]");
    }
}
