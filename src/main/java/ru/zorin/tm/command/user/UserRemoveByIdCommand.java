package ru.zorin.tm.command.user;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "remove-user-by-login";
    }

    @Override
    public String description() {
        return "Delete user using login";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeByLogin(userId, login);
        if (user == null) throw new InvalidLoginException();
        System.out.println("[REMOVED]");
        serviceLocator.getAuthService().logout();
    }
}