package ru.zorin.tm.command.user;

import ru.zorin.tm.entity.User;

public class UserShow {
    static void showUser(User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }
}