package ru.zorin.tm.command.user;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.util.TerminalUtil;

import static ru.zorin.tm.command.user.UserShow.showUser;

public class UserShowByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-user-by-id";
    }

    @Override
    public String description() {
        return "Show information about user by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[ERROR]");
            return;
        }
        showUser(user);
        System.out.println("[COMPLETE]");
    }
}
