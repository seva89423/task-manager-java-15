package ru.zorin.tm.command.user;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.util.TerminalUtil;

import static ru.zorin.tm.command.user.UserShow.showUser;

public class UserShowByLoginCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-user-by-login";
    }

    @Override
    public String description() {
        return "Show information about user by login";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) {
            System.out.println("[ERROR]");
            return;
        }
        showUser(user);
        System.out.println("[COMPLETE]");
    }
}