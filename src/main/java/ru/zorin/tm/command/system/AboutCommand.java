package ru.zorin.tm.command.system;

import ru.zorin.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show info about developer";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }
}