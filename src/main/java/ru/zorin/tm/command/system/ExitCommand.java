package ru.zorin.tm.command.system;

import ru.zorin.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit application";
    }

    @Override
    public void execute() {
      System.exit(0);
    }
}
