package ru.zorin.tm.command.project;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.util.TerminalUtil;

import static ru.zorin.tm.command.project.ProjectShow.showProject;

public class ProjectShowByIndexCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-project-by-index";
    }

    @Override
    public String description() {
        return "Show project using index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Project project = serviceLocator.getProjectService().findProjectByIndex(userId, index);
            if (project == null) throw new ProjectEmptyException();
            showProject(project);
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }
}