package ru.zorin.tm.command.project;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.util.TerminalUtil;

import static ru.zorin.tm.command.project.ProjectShow.showProject;

public class ProjectShowByNameCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-project-by-name";
    }

    @Override
    public String description() {
        return "Show project using name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        showProject(project);
        System.out.println("[COMPLETE]");
    }
}
