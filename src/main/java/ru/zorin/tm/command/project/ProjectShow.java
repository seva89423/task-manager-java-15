package ru.zorin.tm.command.project;


import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.project.ProjectEmptyException;

public class ProjectShow {
    static void showProject(Project project) {
        if (project == null) throw new ProjectEmptyException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }
}