package ru.zorin.tm.command;

import ru.zorin.tm.api.service.ServiceLocator;
import ru.zorin.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {

    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();
}