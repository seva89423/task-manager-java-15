package ru.zorin.tm.error.invalid;

public class InvalidIdException extends RuntimeException {

    public InvalidIdException() {
        super("Invalid id");
    }
}